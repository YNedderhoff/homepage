---
layout: home
excerpt: Ich bin Student im M.Sc. Computational Linguistics an der Universität Stuttgart.
tags: [yanick, nedderhoff, computerlinguist, computerlinguistik, computational linguist, computational linguistics, natural language processing, maschinelle sprachverarbeitung, universität stuttgart, lebenslauf, cv, curriculum vitae, beruflicher werdegang, ausbildung, skills, fähigkeiten, kenntnisse, sprachkenntnisse, edv-kenntnisse, programme, software]
image:
  feature: indian-country-wide.jpg
  credit: #Y. Nedderhoff
  creditlink: #http://yanicknedderhoff.com
share: false
---
